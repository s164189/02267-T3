package calculator;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class CalculatorTest {
    private String input;
    Calculator cal = new Calculator();


    @Given("the input is {string}")
    public void the_input_is(String string) {
        input = string;
    }

    @Then("output for add is {int}")
    public void outputForAddIs(Integer result) {
        assertThat(cal.add(input), is(equalTo(result)));
    }
}