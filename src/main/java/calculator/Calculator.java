package calculator;


import static com.sun.org.apache.xalan.internal.lib.ExsltStrings.split;

public class Calculator {
    public int add(String input){
        int sum = 0;
        String[] comma_seperated = input.split("[^\\d|.]");

        for (String str: comma_seperated
             ) {
            try {
                sum += Double.valueOf(str);
            } catch (NumberFormatException e) {
                sum += 0;
            }
        }
        return sum;
    }
}
