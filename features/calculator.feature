Feature: Calculator addition
  Scenario: the string is empty
    Given the input is " "
    Then output for add is 0

  Scenario: the string is "1"
    Given the input is "1"
    Then output for add is 1

  Scenario: the string is "1,2"
    Given the input is "1,2"
    Then output for add is 3

  Scenario: the string is "1\gtn2"
    Given the input is "1\n2"
    Then output for add is 3

  Scenario: the string is "1\n2,3"
    Given the input is "1\n2,3"
    Then output for add is 6

  Scenario: the string is "//;\n1\n2,3"
    Given the input is "//;\n1\n2,3"
    Then output for add is 6